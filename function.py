import dnstwist
import os
import homoglyphs as hg
import json


def genTldsDnstwist(domain):
    """Generate a file and upload to S3 with dnstwist 
        :param domain: list of domain
        :param organisation_name: organisation name
    """
    return dnstwist.run(domain=f"{domain}", registered=True, format='null')

def homoglyphGenerator(domain):
    homoglyphs = hg.Homoglyphs(categories=('ARMENIAN', 'BUGINESE', 'CARIAN', 'CHEROKEE', 'CYRILLIC', 'GREEK', 'LATIN')).get_combinations(domain)
    list_homoglyphs = []
    for element in homoglyphs:
        list_homoglyphs.append(str(element))
    return list_homoglyphs



