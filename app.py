from chalice import Chalice, Response
import dnstwist
import json
import gzip
from function import genTldsDnstwist, homoglyphGenerator
from threading import Thread

app = Chalice(app_name='i360Extern')
app.api.binary_types.append('application/json')


@app.route('/dnstwist/{domain}', methods=['GET'])
def get_dnstwist(domain):
    try:
        result = json.dumps(genTldsDnstwist(domain)).encode('utf-8')
        custom_headers = {
        'Content-Type': 'application/json',
        }
        return Response(body=result,
                    status_code=200,
                    headers=custom_headers
        )
    except Exception as err:
        print(err)
        
@app.route('/homoglyph/{domain}', methods=['GET'])
def get_homoglyph(domain):
    try:
        result = json.dumps(homoglyphGenerator(domain)).encode('utf-8')
        custom_headers = {
        'Content-Type': 'application/json',
        }
        return Response(body=result,
                    status_code=200,
                    headers=custom_headers
        )
    except Exception as err:
        print(err)



# The view function above will return {"hello": "world"}
# whenever you make an HTTP GET request to '/'.
#
# Here are a few more examples:
#
# @app.route('/hello/{name}')
# def hello_name(name):
#    # '/hello/james' -> {"hello": "james"}
#    return {'hello': name}
#
# @app.route('/users', methods=['POST'])
# def create_user():
#     # This is the JSON body the user sent in their POST request.
#     user_as_json = app.current_request.json_body
#     # We'll echo the json body back to the user in a 'user' key.
#     return {'user': user_as_json}
#
# See the README documentation for more examples.
#
